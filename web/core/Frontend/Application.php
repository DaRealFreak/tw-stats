<?php

namespace TwStats\Core\Frontend;

use TwStats\Core\Backend\Database;
use TwStats\Core\Backend\RequestHandler;
use TwStats\Core\Backend\SystemEnvironmentBuilder;
use TwStats\Core\General\ApplicationInterface;
use TwStats\Core\General\SettingManager;
use TwStats\Core\Utility\GeneralUtility;

class Application implements ApplicationInterface
{
    /**
     * setting manager
     *
     * @var SettingManager|null
     */
    private $settingManager = null;
    /**
     * database connection
     *
     * @var Database|null
     */
    private $database = null;

    /**
     * frontend handler
     *
     * @var Twig|null
     */
    private $frontendHandler = null;

    /**
     * @var RequestHandler|null
     */
    private $requestHandler = null;

    /**
     * Constructor setting up legacy constant and register available Request Handlers
     *
     * @param \Composer\Autoload\ClassLoader $classLoader an instance of the class loader
     */
    public function __construct($classLoader)
    {
        session_start();
        /*
         * run the environmental builder
         */
        SystemEnvironmentBuilder::run();
        /*
         * initialize the setting manager
         */
        $GLOBALS['SETTINGS'] = $this->settingManager = GeneralUtility::makeInstance(SettingManager::class);
        /*
         * initialize the database directly
         */
        $GLOBALS['DB'] = $this->database = GeneralUtility::makeInstance(Database::class);
        /*
         * initialize the frontend handler
         */
        $GLOBALS['FE'] = $this->frontendHandler = GeneralUtility::makeInstance(Twig::class);
        /*
         * initialize the html purifier with the default configuration
         */
        $config = \HTMLPurifier_Config::createDefault();
        $GLOBALS['purifier'] = new \HTMLPurifier($config);
        /*
         * initialize the request handler
         */
        $this->requestHandler = GeneralUtility::makeInstance(RequestHandler::class);
    }

    /**
     * Starting point
     *
     * @param callable $execute
     * @return void
     */
    public function run(callable $execute = null)
    {
        if ($execute !== null) {
            call_user_func($execute);
        }

        GeneralUtility::makeInstance($this->requestHandler->getRequestedClass());
    }
}
